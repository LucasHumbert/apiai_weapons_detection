import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import InfosView from '../views/InfosView.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView
    },
    {
        path: '/infos/:id',
        name: 'infos',
        component: InfosView
    },
    {
        path: "*",
        component: HomeView
    }
]

const router = new VueRouter({
    routes
})

export default router
